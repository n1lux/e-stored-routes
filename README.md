# E-stored

Projeto de api para cadastro de mapas e rotas visando o calculo e retorno da rota com menor custo.

## Como criar o ambiente?

1. Clone o repositório
2. Crie um virtualenv com Python 3.6
3. Ative o seu virtualev.
4. Instale as dependências.
5. Exetute as migrações
6. Execute os testes.


```console
git clone https://n1lux@bitbucket.org/n1lux/e-stored-routes.git
cd e-stored-routes
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python manage.py makemigrations api
python manage.py migrate
python manage.py test
```

## Como executar o servidor

```console
python manage.py runserver
```

## Como testar os serviços
Para cadastrar o mapa e suas rotas é necessario realizar uma requisição com o metodo post para a url /api/v0/maps/route/.

## Formato do arquivo json.
```console
{
    "map": {
        "name": "TestMap",
        "routes": [
               "A B 10",
               "B D 15",
               "A C 20",
               "C D 30",
               "B E 50",
               "D E 30"
      ]
    }
}

```

## Exemplo da requisição utilizando o utilitario curl.
```bash
curl -H "Content-Type: application/json" -X POST http://localhost:8000/api/v0/maps/route/ -d '
{
    "map": {
        "name": "TestMap",
        "routes": [
               "A B 10",
               "B D 15",
               "A C 20",
               "C D 30",
               "B E 50",
               "D E 30"
      ]
    }
}'

```


#### Para testar o servico que calcula a rota mais economica e necessário realizar uma requisição com metodo POST para a url /api/v0/users/economy/

Formato do arquivo Json
```bash
{
  "map": {
    "name": "TestMap",
    "route": {
       "origin": "A",
       "destiny": "D",
       "autonomy": "10",
       "vl": "2,50"
     }
  }
}
```


#### Exemplo de requisição que retorna a rota mais economica utilizando o utilitario curl.
```bash
curl -H "Content-Type: application/json" -X POST http://localhost:8000/api/v0/users/economy/ -d '
{
  "map": {
    "name": "TestMap",
    "route": {
       "origin": "A",
       "destiny": "D",
       "autonomy": "10",
       "vl": "2,50"
     }
  }
}'

```

#### O retorno será uma string contendo a rota e o valor o custo.
Ex.
"ABD com custo 6.25"