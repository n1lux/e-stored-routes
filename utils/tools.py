from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import APIException


def get_economic_route(metrics):
    """
    This function receive route metrics and prepare return data
    :param metrics: Route metrics calculate by routes_metric
    :return:
    """
    better = metrics
    route = better['route']
    val = better['cost']
    a, b = route
    return "{}{}{} com custo {}".format(a.origin, b.origin, b.destiny, val)


def routes_metric(routes, autonomy, val_l):
    """
    This function receive candidate routes and make calcs and return a ordered list by cost
    :param routes: Routes canditades found by get_routes
    :param autonomy: Autonomy value received by post
    :param val_l: Value of fuel received by post
    :return: Ordered list by cost
    """
    metrics = []
    for r in routes:
        d = calc_distance(r)
        c = calc_cost(d, autonomy, val_l)
        metrics.append({'route': r, 'distance': d, 'cost': c})
    return sorted(metrics, key=lambda k: k['cost'])


def get_routes(map, orig, dest):
    """
    This function receive stored map and origin, destiny
    :param map: Map stored previus
    :param orig: Origin from user map in post
    :param dest: Destiny from user map in post
    :return: All possible routes
    """

    all_orig = map._routes.filter(origin=orig)
    all_dest = map._routes.filter(destiny=dest)

    full_routes = []
    for r1 in all_orig:
        for r2 in all_dest:
            if r1.destiny == r2.origin:
                full_routes.append((r1, r2))

    return full_routes


def calc_distance(route):
    """
    This function calculate the distance of route points (a -> b)
    :param route: Possible route from get_routes
    :return: Distance of route point
    """
    ra, rb = route
    return ra.distance + rb.distance


def calc_cost(dist, autonomy, vl):
    """
    This function calculate the cost for route
    :param dist: Distance between two points for route
    :param autonomy: Value received from user map
    :param vl: Value of fuel liter
    :return: value calculate with distance, fuel value and autonomy
    """
    return (dist * vl) / autonomy


def route_same_point(map_, orig, dest):
    """
    This function check if orig and dest is same point
    :param map_:
    :param orig: Origin point
    :param dest: Destiny point
    :return:
    """
    try:
        in_route = map_._routes.get(origin=orig, destiny=dest)
    except ObjectDoesNotExist:
        return None

    return in_route