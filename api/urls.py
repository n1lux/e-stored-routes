from django.conf.urls import url, include
from api.views import api_root, MapList, Route, UsersEconomy

urlpatterns = [
    url(r'^$', api_root, name='api_root'),
    url(r'^users/', include([
        url(r'^economy/$', UsersEconomy.as_view(), name='economy'),
    ])),
    url(r'^maps/', include([
        url(r'^$', MapList.as_view(), name='maps'),
        url(r'^route/$', Route.as_view(), name='route'),
    ])),
]
