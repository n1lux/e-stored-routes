from django.db import models
import uuid
# Create your models here.


class BasicModel(models.Model):
    """
    Basic Model
    """
    uid = models.UUIDField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    @classmethod
    def create(cls, **kwargs):
        kwargs['uid'] = uuid.uuid4().hex

        model = cls(**kwargs)
        model.save()
        return model


class Map(BasicModel):
    """
    This class represent a Map with name
    :param name: Map name
    """
    name = models.CharField(max_length=10, null=False, blank=False)


class Routes(BasicModel):
    """
    This class store the maps routes
    :param origin: Origin point
    :param destiny: Destiny point
    :param distance: Points distance
    :param created_at: Datatime when point was created
    """
    origin = models.CharField(max_length=1, null=False, blank=False)
    destiny = models.CharField(max_length=1, null=False, blank=False)
    distance = models.PositiveIntegerField(null=False, blank=False)
    map_refer = models.ForeignKey(Map, related_name='_routes')

    class Meta:
        verbose_name = 'route'
        verbose_name_plural = 'routes'
        ordering = ('created_at', )

    def __str__(self):
        return '{} -> {} : {}'.format(self.origin, self.destiny, self.distance)


