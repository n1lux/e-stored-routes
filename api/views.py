from rest_framework.views import APIView
# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from api.models import Map, Routes
from api.serializers import MapSerializer, RouteSerializer
from utils.tools import get_routes, routes_metric, get_economic_route, route_same_point


@api_view(['GET'])
def api_root(request):
    return Response()


class MapList(APIView):
    def get(self, request):
        return Response()

    def post(self, request):
        try:
            map_name = request.data['name']
        except KeyError:
            return Response(data={'message': "Invalid params!"}, status=status.HTTP_400_BAD_REQUEST)

        if not map_name:
            return Response(data={'message': "Empty map name!"}, status=status.HTTP_400_BAD_REQUEST)

        map = Map.create(name=map_name)
        map_serializer = MapSerializer(map, many=False)

        return Response(data=map_serializer.data, status=status.HTTP_201_CREATED)


class Route(APIView):
    def get(self, request):
        return Response()

    def post(self, request):
        map_routes = []
        try:
            map_name = request.data['map']['name']
            routes = request.data['map']['routes']
        except KeyError:
            return Response(data={'message': "Invalid map format!"}, status=status.HTTP_400_BAD_REQUEST)

        m = Map.create(name=map_name)
        for r in routes:
            try:
                orig, dest, dist = r.split()
            except ValueError:
                return Response(data={'message': "Invalid routes format!"}, status=status.HTTP_400_BAD_REQUEST)

            route = Routes.create(origin=orig, destiny=dest, distance=dist, map_refer=m)
            map_routes.append(route)

        route_serializer = RouteSerializer(map_routes, many=True)

        return Response(data=route_serializer.data, status=status.HTTP_201_CREATED)


class UsersEconomy(APIView):
    def get(self, request):
        return Response()

    def post(self, request):
        try:
            map_ = request.data['map']
            map_name = map_['name']
            route = map_['route']
            origin = route['origin']
            destiny = route['destiny']
        except KeyError:
            return Response(data={'message': "Invalid map format!"}, status=status.HTTP_400_BAD_REQUEST)

        if not map_name:
            return Response(data={'message': "Invalid map name value!"}, status=status.HTTP_400_BAD_REQUEST)


        try:
            val_l = float(route['vl'])
        except ValueError:
            val_l = float(route['vl'].replace(',', '.'))

        try:
            autonomy = int(route['autonomy'])
        except ValueError:
            return Response(data={'message': "Invalid autonomy value!"}, status=status.HTTP_400_BAD_REQUEST)

        map_obj = Map.objects.get(name=map_name)

        same = route_same_point(map_=map_obj, orig=origin, dest=destiny)

        if not same:
            routes = get_routes(map_obj, orig=origin, dest=destiny)

            try:
                metrics = routes_metric(routes=routes, autonomy=autonomy, val_l=val_l)[0]
            except IndexError:
                return Response(data={'message': "Route not found!"}, status=status.HTTP_400_BAD_REQUEST)

            data = get_economic_route(metrics)
        else:
            cost = (same.distance * val_l) / autonomy
            data = "{}{} com custo {}".format(origin, destiny, format(cost, '.2f'))


        return Response(data=data, status=status.HTTP_200_OK)


