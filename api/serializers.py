from rest_framework import serializers
from api.models import Map, Routes


class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Map
        fields = ('uid', 'name',)


class RouteSerializer(serializers.ModelSerializer):
    map_refer = MapSerializer()
    class Meta:
        model = Routes
        fields = ('uid', 'origin', 'destiny', 'distance', 'map_refer')

