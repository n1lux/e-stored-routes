import json
from rest_framework.test import APITestCase
from rest_framework import status
from api.models import Map, Routes


class TestMap(APITestCase):
    def setUp(self):
        self.map = {'name': 'TestMap'}
        self.res = self.client.post(path='/api/v0/maps/', data=self.map, format='json')

    def test_create_map_status_code(self):
        """ Post an correct map """
        self.assertEqual(self.res.status_code, status.HTTP_201_CREATED)

    def test_create_map_instance(self):
        """ Test map creation, post a map json and check object Map instances """

        with self.subTest():
            self.assertEqual(Map.objects.count(), 1)
            self.assertEqual(Map.objects.get().name, 'TestMap')

    def test_map_response(self):
        """
        Must be return map data and test key in response
        :return:
        """

        self.assertIn('name', json.loads(self.res.content))

    def test_map_invalid(self):
        map = {'xpto': 'TestMap'}
        res = self.client.post(path='/api/v0/maps/', data=map, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_map_name_empty(self):
        map = {'name': ''}
        res = self.client.post(path='/api/v0/maps/', data=map, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


class TestRoutes(APITestCase):
    def setUp(self):
        self.map_ = {
            'map': {
                'name': 'TestMap',
                'routes': ["A B 10",
                           "B D 15",
                           "A C 20",
                           "C D 30",
                           "B E 50",
                           "D E 30",
                           ],
            }
        }

    def test_create_routes(self):
        res = self.client.post(path='/api/v0/maps/route/', data=self.map_, format='json')

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        with self.subTest():
            for r in json.loads(res.content):
                self.assertIn('map_refer', r)
                self.assertIn('origin', r)
                self.assertIn('destiny', r)
                self.assertIn('distance', r)

    def test_invalid_routes(self):
        map_ = {
            'map': {
                'name': 'TestMap',
                'routes': ["A B10",
                           "B D 15",
                           "A C 20",
                           "CD 30",
                           "B E 50",
                           "D E 30",
                           ],
            }
        }

        res = self.client.post(path='/api/v0/maps/route/', data=map_, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)


class TestUserMap(APITestCase):
    def setUp(self):
        self.map_ = {
            'map': {
                'name': 'TestMap',
                'routes': ["A B 10",
                           "B D 15",
                           "A C 20",
                           "C D 30",
                           "B E 50",
                           "D E 30",
                           ],
            }
        }
        res = self.client.post(path='/api/v0/maps/route/', data=self.map_, format='json')

    def test_has_map_routes(self):
        self.assertTrue(Routes.objects.count() > 1)

    def test_economy(self):
        user_map = {
            'map': {
                'name': 'TestMap',
                'route': {
                    'origin': 'A',
                    'destiny': 'D',
                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual("ABD com custo 6.25", json.loads(res.content))

    def test_invalid_user_map(self):
        user_map = {
            'map': {
                'name': 'TestMap',
                'route': {
                    'origin': 'A',

                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_user_map_without_name(self):
        user_map = {
            'map': {
                'name': '',
                'route': {
                    'origin': 'A',
                    'destiny': 'D',
                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_route_only_way_a_b(self):
        user_map = {
            'map': {
                'name': 'TestMap',
                'route': {
                    'origin': 'A',
                    'destiny': 'B',
                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual("AB com custo 2.50", json.loads(res.content))

    def test_route_only_way_a_c(self):
        user_map = {
            'map': {
                'name': 'TestMap',
                'route': {
                    'origin': 'A',
                    'destiny': 'C',
                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual("AC com custo 5.00", json.loads(res.content))

    def test_route_same_points(self):
        user_map = {
            'map': {
                'name': 'TestMap',
                'route': {
                    'origin': 'A',
                    'destiny': 'A',
                    'autonomy': '10',
                    'vl': '2,50',
                }
            }
        }

        res = self.client.post('/api/v0/users/economy/', data=user_map, format='json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

