from rest_framework.test import APITestCase
from rest_framework import status


class APITests(APITestCase):
    def setUp(self):
        pass

    def test_index_api_uri(self):
        """" Must be return http status 200 """
        res = self.client.get('/api/v0/')

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_maps_resource_uri(self):
        """ Must be return http status 200 """
        res = self.client.get('/api/v0/maps/')

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_routes_resource_uri(self):
        """ Must be return http status 200 """
        res = self.client.get('/api/v0/maps/route/')

        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_user_economy_resource_uri(self):
        res = self.client.get('/api/v0/users/economy/')
        self.assertEqual(res.status_code, status.HTTP_200_OK)


